/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aplikasihperpustakaan;

import javax.swing.JOptionPane;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author ASUS
 */
public class MainView extends javax.swing.JFrame{

    /**
     * Creates new form MainView
     */
    private Connection koneksi;
    private Statement stm;
    private String buku;
    
    public MainView() {
        initComponents();
        getTablePetugas();
    }
    
    public void getTablePetugas(){
        DefaultTableModel tblMod = new DefaultTableModel();
        
        tblMod.addColumn("Nama");
        tblMod.addColumn("NIM");
        tblMod.addColumn("Username");
        tblMod.addColumn("Password");
        tabelUser.setModel(tblMod);
        
        try {
            koneksi = new Koneksi().koneksiDB();
            String query = "SELECT * FROM admin";
            stm = koneksi.createStatement();
            ResultSet rs = stm.executeQuery(query);
         
            while(rs.next()){
                tblMod.addRow(new Object[]{
                    rs.getString("NamaUser"),
                    rs.getString("NIP"),
                    rs.getString("Username"),
                    rs.getString("PasswordUser")
                
                });
                tabelUser.setModel(tblMod);
            }
        } catch (Exception e) {}
    }

        public void getTableAnggota(){
        DefaultTableModel tblMod = new DefaultTableModel();
        
        tblMod.addColumn("Nama");
        tblMod.addColumn("NIM");
        tblMod.addColumn("Username");
        tblMod.addColumn("Password");
        tabelAnggota.setModel(tblMod);
        
        try {
            koneksi = new Koneksi().koneksiDB();
            String query = "SELECT * FROM anggota";
            stm = koneksi.createStatement();
            ResultSet rs = stm.executeQuery(query);
         
            while(rs.next()){
                tblMod.addRow(new Object[]{
                    rs.getString("NamaAnggota"),
                    rs.getString("NIM"),
                    rs.getString("UsernameAnggota"),
                    rs.getString("PasswordAnggota")
                
                });
                tabelAnggota.setModel(tblMod);
            }
        } catch (Exception e) {}
    }

    public void getTableBuku(){
        DefaultTableModel tblMod = new DefaultTableModel();
        
        tblMod.addColumn("Kode Buku");
        tblMod.addColumn("Judul Buku");
        tblMod.addColumn("Pengarang");
        tblMod.addColumn("Penerbit");
        tabelBuku.setModel(tblMod);
        
        try {
            koneksi = new Koneksi().koneksiDB();
            String query = "SELECT * FROM buku";
            stm = koneksi.createStatement();
            ResultSet rs = stm.executeQuery(query);
            
            while(rs.next()){
                tblMod.addRow(new Object[]{
                    rs.getString("KodeBuku"),
                    rs.getString("JudulBuku"),
                    rs.getString("Pengarang"),
                    rs.getString("penerbit")
                });
                tabelBuku.setModel(tblMod);
            }
        } catch (Exception e) {}
    }
    
    public void getTablePeminjam(){
        DefaultTableModel tblMod = new DefaultTableModel();
        
        tblMod.addColumn("Kode Buku");
        tblMod.addColumn("Judul Buku");
        tblMod.addColumn("NIM");
        tblMod.addColumn("Tanggal Pinjam");
        tblMod.addColumn("Tanggal Kembali");
        tabelPeminjam.setModel(tblMod);
        
        try {
            koneksi = new Koneksi().koneksiDB();
            String query = "SELECT * FROM peminjaman";
            stm = koneksi.createStatement();
            ResultSet rs = stm.executeQuery(query);
         
            while(rs.next()){
                tblMod.addRow(new Object[]{
                    rs.getString("KodeBuku"),
                    rs.getString("JudulBuku"),
                    rs.getString("TglPinjam"),
                    rs.getString("TglKembali")
                
                });
                tabelPeminjam.setModel(tblMod);
            }
        } catch (Exception e) {}
    }
    public void getTableKembali(){
        DefaultTableModel tblMod = new DefaultTableModel();
        
        tblMod.addColumn("Kode Buku");
        tblMod.addColumn("Judul Buku");
        tblMod.addColumn("NIM");
        tblMod.addColumn("Tanggal Pinjam");
        tblMod.addColumn("Tanggal Kembali");
        tblMod.addColumn("Status");
        tabelKembali.setModel(tblMod);
        
        try {
            koneksi = new Koneksi().koneksiDB();
            String query = "SELECT * FROM peminjaman";
            stm = koneksi.createStatement();
            ResultSet rs = stm.executeQuery(query);
         
            while(rs.next()){
                tblMod.addRow(new Object[]{
                    rs.getString("KodeBuku"),
                    rs.getString("JudulBuku"),
                    rs.getString("TglPinjam"),
                    rs.getString("TglKembali"),
                    rs.getString("Status")
                
                });
                tabelKembali.setModel(tblMod);
            }
        } catch (Exception e) {}
    }
    
    public void getTableLaporan(){
        DefaultTableModel tblMod = new DefaultTableModel();
        
        tblMod.addColumn("Kode Buku");
        tblMod.addColumn("Judul Buku");
        tblMod.addColumn("NIM");
        tblMod.addColumn("Tanggal Pinjam");
        tblMod.addColumn("Tanggal Kembali");
        tblMod.addColumn("Status");
        tabelLaporan.setModel(tblMod);
        
        try {
            koneksi = new Koneksi().koneksiDB();
            String query = "SELECT * FROM peminjaman";
            stm = koneksi.createStatement();
            ResultSet rs = stm.executeQuery(query);
         
            while(rs.next()){
                tblMod.addRow(new Object[]{
                   
                   rs.getString("KodeBuku"),
                    rs.getString("JudulBuku"),
                    rs.getString("TglPinjam"),
                    rs.getString("TglKembali"),
                    rs.getString("Status")
                                
                });
                tabelLaporan.setModel(tblMod);
            }
        } catch (Exception e) {}
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        bodyPanel = new javax.swing.JPanel();
        menuPanel = new javax.swing.JPanel();
        btnuser = new javax.swing.JButton();
        btnBuku = new javax.swing.JButton();
        btnPinjam = new javax.swing.JButton();
        btnKembali = new javax.swing.JButton();
        btnLapor = new javax.swing.JButton();
        btnLogout = new javax.swing.JButton();
        btnAnggota = new javax.swing.JButton();
        jLabel27 = new javax.swing.JLabel();
        jLabel28 = new javax.swing.JLabel();
        jLabel29 = new javax.swing.JLabel();
        mainPanel = new javax.swing.JPanel();
        userPanel = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        namaUser = new javax.swing.JTextField();
        nipUser = new javax.swing.JTextField();
        usernameAdmin = new javax.swing.JTextField();
        passUser = new javax.swing.JPasswordField();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        addUser = new javax.swing.JButton();
        editUser = new javax.swing.JButton();
        simpanUser = new javax.swing.JButton();
        hapusUser = new javax.swing.JButton();
        cariTextUser = new javax.swing.JTextField();
        cariUser = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        tabelUser = new javax.swing.JTable();
        bukuPanel = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        jLabel18 = new javax.swing.JLabel();
        kodeBuku = new javax.swing.JTextField();
        judulBuku = new javax.swing.JTextField();
        pengarangBuku = new javax.swing.JTextField();
        penerbitBuku = new javax.swing.JTextField();
        addBuku = new javax.swing.JButton();
        editBuku = new javax.swing.JButton();
        simpanBuku = new javax.swing.JButton();
        hapusBuku = new javax.swing.JButton();
        cariTextBuku = new javax.swing.JTextField();
        cariBuku = new javax.swing.JButton();
        jScrollPane3 = new javax.swing.JScrollPane();
        tabelBuku = new javax.swing.JTable();
        pinjamPanel = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        kodeBukupinjam = new javax.swing.JTextField();
        jLabel19 = new javax.swing.JLabel();
        jLabel20 = new javax.swing.JLabel();
        jLabel21 = new javax.swing.JLabel();
        judulBukupinjam = new javax.swing.JTextField();
        NIMpeminjam = new javax.swing.JTextField();
        jLabel22 = new javax.swing.JLabel();
        tanggalPinjam = new javax.swing.JTextField();
        addBukupinjam = new javax.swing.JButton();
        editBukupinjam = new javax.swing.JButton();
        simpanBukupinjam = new javax.swing.JButton();
        hapusBukupinjam = new javax.swing.JButton();
        cariBukupinjam = new javax.swing.JButton();
        cariTextBukupinjam = new javax.swing.JTextField();
        jScrollPane4 = new javax.swing.JScrollPane();
        tabelPeminjam = new javax.swing.JTable();
        tanggalKembali = new javax.swing.JLabel();
        penerbitBuku2 = new javax.swing.JTextField();
        kembaliPanel = new javax.swing.JPanel();
        jLabel6 = new javax.swing.JLabel();
        jLabel23 = new javax.swing.JLabel();
        jLabel24 = new javax.swing.JLabel();
        jLabel25 = new javax.swing.JLabel();
        jLabel26 = new javax.swing.JLabel();
        tanggalKembali1 = new javax.swing.JLabel();
        tanggalkembaliin = new javax.swing.JTextField();
        tanggalkembali = new javax.swing.JTextField();
        NIMkembali = new javax.swing.JTextField();
        judulBukukembali = new javax.swing.JTextField();
        kodeBukukembali = new javax.swing.JTextField();
        addBukukembali = new javax.swing.JButton();
        editBukukembali = new javax.swing.JButton();
        simpanBukukembali = new javax.swing.JButton();
        hapusBukukembali = new javax.swing.JButton();
        cariTextBukukembali = new javax.swing.JTextField();
        cariBukukembali = new javax.swing.JButton();
        jScrollPane5 = new javax.swing.JScrollPane();
        tabelKembali = new javax.swing.JTable();
        tanggalKembali2 = new javax.swing.JLabel();
        Denda = new javax.swing.JTextField();
        laporPanel = new javax.swing.JPanel();
        jLabel7 = new javax.swing.JLabel();
        jScrollPane6 = new javax.swing.JScrollPane();
        tabelLaporan = new javax.swing.JTable();
        anggotaPanel = new javax.swing.JPanel();
        jLabel8 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        namaAnggota = new javax.swing.JTextField();
        jLabel12 = new javax.swing.JLabel();
        nimAnggota = new javax.swing.JTextField();
        jLabel13 = new javax.swing.JLabel();
        usernameAnggota = new javax.swing.JTextField();
        jLabel14 = new javax.swing.JLabel();
        passAnggota = new javax.swing.JPasswordField();
        addAnggota = new javax.swing.JButton();
        editAnggota = new javax.swing.JButton();
        simpanAnggota = new javax.swing.JButton();
        hapusAnggota = new javax.swing.JButton();
        cariAnggota = new javax.swing.JButton();
        cariTextAnggota = new javax.swing.JTextField();
        jScrollPane2 = new javax.swing.JScrollPane();
        tabelAnggota = new javax.swing.JTable();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Aplikasi Perpustakaan");

        bodyPanel.setBackground(new java.awt.Color(0, 102, 102));

        menuPanel.setBackground(new java.awt.Color(0, 102, 102));
        menuPanel.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED, java.awt.Color.white, java.awt.Color.white, java.awt.Color.white, java.awt.Color.white));

        btnuser.setFont(new java.awt.Font("Times New Roman", 0, 12)); // NOI18N
        btnuser.setIcon(new javax.swing.ImageIcon(getClass().getResource("/aplikasihperpustakaan/icons8-administrator-male-32.png"))); // NOI18N
        btnuser.setText("PETUGAS");
        btnuser.setHideActionText(true);
        btnuser.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        btnuser.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnuserActionPerformed(evt);
            }
        });

        btnBuku.setFont(new java.awt.Font("Times New Roman", 0, 12)); // NOI18N
        btnBuku.setIcon(new javax.swing.ImageIcon(getClass().getResource("/aplikasihperpustakaan/icons8-book-shelf-32.png"))); // NOI18N
        btnBuku.setText("BUKU");
        btnBuku.setHideActionText(true);
        btnBuku.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        btnBuku.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBukuActionPerformed(evt);
            }
        });

        btnPinjam.setFont(new java.awt.Font("Times New Roman", 0, 12)); // NOI18N
        btnPinjam.setIcon(new javax.swing.ImageIcon(getClass().getResource("/aplikasihperpustakaan/icons8-return-book-32.png"))); // NOI18N
        btnPinjam.setText("PEMINJAMAN");
        btnPinjam.setHideActionText(true);
        btnPinjam.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        btnPinjam.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPinjamActionPerformed(evt);
            }
        });

        btnKembali.setFont(new java.awt.Font("Times New Roman", 0, 12)); // NOI18N
        btnKembali.setIcon(new javax.swing.ImageIcon(getClass().getResource("/aplikasihperpustakaan/icons8-borrow-book-32.png"))); // NOI18N
        btnKembali.setText("PENGEMBALIAN");
        btnKembali.setHideActionText(true);
        btnKembali.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        btnKembali.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnKembaliActionPerformed(evt);
            }
        });

        btnLapor.setFont(new java.awt.Font("Times New Roman", 0, 12)); // NOI18N
        btnLapor.setIcon(new javax.swing.ImageIcon(getClass().getResource("/aplikasihperpustakaan/icons8-pass-fail-32.png"))); // NOI18N
        btnLapor.setText("LAPORAN");
        btnLapor.setHideActionText(true);
        btnLapor.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        btnLapor.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLaporActionPerformed(evt);
            }
        });

        btnLogout.setFont(new java.awt.Font("Times New Roman", 0, 12)); // NOI18N
        btnLogout.setIcon(new javax.swing.ImageIcon(getClass().getResource("/aplikasihperpustakaan/icons8-exit-32.png"))); // NOI18N
        btnLogout.setText("LOGOUT");
        btnLogout.setHideActionText(true);
        btnLogout.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        btnLogout.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLogoutActionPerformed(evt);
            }
        });

        btnAnggota.setFont(new java.awt.Font("Times New Roman", 0, 12)); // NOI18N
        btnAnggota.setIcon(new javax.swing.ImageIcon(getClass().getResource("/aplikasihperpustakaan/icons8-customer-32.png"))); // NOI18N
        btnAnggota.setText("ANGGOTA");
        btnAnggota.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        btnAnggota.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAnggotaActionPerformed(evt);
            }
        });

        jLabel27.setForeground(new java.awt.Color(255, 255, 255));
        jLabel27.setIcon(new javax.swing.ImageIcon(getClass().getResource("/aplikasihperpustakaan/icons8-library-100.png"))); // NOI18N

        jLabel28.setFont(new java.awt.Font("Times New Roman", 1, 20)); // NOI18N
        jLabel28.setForeground(new java.awt.Color(255, 255, 255));
        jLabel28.setText("MEMBACA");

        jLabel29.setFont(new java.awt.Font("Times New Roman", 1, 20)); // NOI18N
        jLabel29.setForeground(new java.awt.Color(255, 255, 255));
        jLabel29.setText("BUDAYAKAN");

        javax.swing.GroupLayout menuPanelLayout = new javax.swing.GroupLayout(menuPanel);
        menuPanel.setLayout(menuPanelLayout);
        menuPanelLayout.setHorizontalGroup(
            menuPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(btnuser, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(btnBuku, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(btnPinjam, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(btnKembali, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(btnLapor, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(btnLogout, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(btnAnggota, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(menuPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(menuPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(menuPanelLayout.createSequentialGroup()
                        .addComponent(jLabel29, javax.swing.GroupLayout.PREFERRED_SIZE, 163, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, menuPanelLayout.createSequentialGroup()
                        .addGap(0, 49, Short.MAX_VALUE)
                        .addGroup(menuPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, menuPanelLayout.createSequentialGroup()
                                .addComponent(jLabel28, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addContainerGap())
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, menuPanelLayout.createSequentialGroup()
                                .addComponent(jLabel27, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(39, 39, 39))))))
        );
        menuPanelLayout.setVerticalGroup(
            menuPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(menuPanelLayout.createSequentialGroup()
                .addComponent(jLabel27, javax.swing.GroupLayout.PREFERRED_SIZE, 123, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel29)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel28)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnuser, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnAnggota, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnBuku, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnPinjam, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnKembali, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnLapor, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnLogout, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(43, 43, 43))
        );

        mainPanel.setBackground(new java.awt.Color(0, 102, 102));
        mainPanel.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED, java.awt.Color.white, java.awt.Color.white, java.awt.Color.white, java.awt.Color.white));
        mainPanel.setLayout(new java.awt.CardLayout());

        userPanel.setBackground(new java.awt.Color(0, 102, 102));

        jLabel3.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(255, 255, 255));
        jLabel3.setText("PETUGAS");

        namaUser.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                namaUserActionPerformed(evt);
            }
        });

        nipUser.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                nipUserActionPerformed(evt);
            }
        });

        passUser.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                passUserActionPerformed(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("Nama ");

        jLabel2.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setText("NIP");

        jLabel9.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel9.setForeground(new java.awt.Color(255, 255, 255));
        jLabel9.setText("Username");

        jLabel10.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel10.setForeground(new java.awt.Color(255, 255, 255));
        jLabel10.setText("Password");

        addUser.setFont(new java.awt.Font("Times New Roman", 0, 12)); // NOI18N
        addUser.setText("Lihat");
        addUser.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addUserActionPerformed(evt);
            }
        });

        editUser.setFont(new java.awt.Font("Times New Roman", 0, 12)); // NOI18N
        editUser.setText("Edit");
        editUser.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                editUserActionPerformed(evt);
            }
        });

        simpanUser.setFont(new java.awt.Font("Times New Roman", 0, 12)); // NOI18N
        simpanUser.setText("Simpan");
        simpanUser.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                simpanUserActionPerformed(evt);
            }
        });

        hapusUser.setFont(new java.awt.Font("Times New Roman", 0, 12)); // NOI18N
        hapusUser.setText("Hapus");
        hapusUser.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                hapusUserActionPerformed(evt);
            }
        });

        cariUser.setFont(new java.awt.Font("Times New Roman", 0, 12)); // NOI18N
        cariUser.setText("Cari");
        cariUser.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cariUserActionPerformed(evt);
            }
        });

        tabelUser.setFont(new java.awt.Font("Times New Roman", 0, 12)); // NOI18N
        tabelUser.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Nama", "NIP", "Username", "Password"
            }
        ));
        jScrollPane1.setViewportView(tabelUser);

        javax.swing.GroupLayout userPanelLayout = new javax.swing.GroupLayout(userPanel);
        userPanel.setLayout(userPanelLayout);
        userPanelLayout.setHorizontalGroup(
            userPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(userPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(userPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(userPanelLayout.createSequentialGroup()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 538, Short.MAX_VALUE)
                        .addContainerGap())
                    .addGroup(userPanelLayout.createSequentialGroup()
                        .addGroup(userPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel3)
                            .addGroup(userPanelLayout.createSequentialGroup()
                                .addGroup(userPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 66, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, 66, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 66, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, 66, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(userPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(namaUser)
                                    .addComponent(nipUser)
                                    .addComponent(usernameAdmin)
                                    .addComponent(passUser))))
                        .addGap(144, 144, 144))
                    .addGroup(userPanelLayout.createSequentialGroup()
                        .addGroup(userPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(userPanelLayout.createSequentialGroup()
                                .addComponent(addUser, javax.swing.GroupLayout.PREFERRED_SIZE, 74, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(editUser, javax.swing.GroupLayout.PREFERRED_SIZE, 72, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(simpanUser, javax.swing.GroupLayout.PREFERRED_SIZE, 77, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(10, 10, 10)
                                .addComponent(hapusUser, javax.swing.GroupLayout.PREFERRED_SIZE, 76, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(userPanelLayout.createSequentialGroup()
                                .addComponent(cariTextUser, javax.swing.GroupLayout.PREFERRED_SIZE, 243, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(cariUser, javax.swing.GroupLayout.PREFERRED_SIZE, 76, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(0, 0, Short.MAX_VALUE))))
        );
        userPanelLayout.setVerticalGroup(
            userPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(userPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel3)
                .addGap(18, 18, 18)
                .addGroup(userPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(namaUser, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(userPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(nipUser, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(userPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(usernameAdmin, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel9))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(userPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(passUser, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel10))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(userPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(addUser)
                    .addComponent(editUser)
                    .addComponent(simpanUser)
                    .addComponent(hapusUser))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(userPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cariTextUser, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cariUser))
                .addGap(51, 51, 51)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 269, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        mainPanel.add(userPanel, "card2");

        bukuPanel.setBackground(new java.awt.Color(0, 102, 51));

        jLabel4.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(255, 255, 255));
        jLabel4.setText("BUKU");

        jLabel15.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel15.setForeground(new java.awt.Color(255, 255, 255));
        jLabel15.setText("Kode Buku");

        jLabel16.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel16.setForeground(new java.awt.Color(255, 255, 255));
        jLabel16.setText("Pengarang");

        jLabel17.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel17.setForeground(new java.awt.Color(255, 255, 255));
        jLabel17.setText("Penerbit");

        jLabel18.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel18.setForeground(new java.awt.Color(255, 255, 255));
        jLabel18.setText("Judul Buku");

        addBuku.setFont(new java.awt.Font("Times New Roman", 0, 12)); // NOI18N
        addBuku.setText("Lihat");
        addBuku.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addBukuActionPerformed(evt);
            }
        });

        editBuku.setFont(new java.awt.Font("Times New Roman", 0, 12)); // NOI18N
        editBuku.setText("Edit");
        editBuku.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                editBukuActionPerformed(evt);
            }
        });

        simpanBuku.setFont(new java.awt.Font("Times New Roman", 0, 12)); // NOI18N
        simpanBuku.setText("Simpan");
        simpanBuku.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                simpanBukuActionPerformed(evt);
            }
        });

        hapusBuku.setFont(new java.awt.Font("Times New Roman", 0, 12)); // NOI18N
        hapusBuku.setText("Hapus");
        hapusBuku.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                hapusBukuActionPerformed(evt);
            }
        });

        cariBuku.setFont(new java.awt.Font("Times New Roman", 0, 12)); // NOI18N
        cariBuku.setText("Cari");
        cariBuku.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cariBukuActionPerformed(evt);
            }
        });

        tabelBuku.setFont(new java.awt.Font("Times New Roman", 0, 12)); // NOI18N
        tabelBuku.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Kode Buku", "Judul Buku", "Pengarang", "Penerbit"
            }
        ));
        jScrollPane3.setViewportView(tabelBuku);
        if (tabelBuku.getColumnModel().getColumnCount() > 0) {
            tabelBuku.getColumnModel().getColumn(3).setHeaderValue("Penerbit");
        }

        javax.swing.GroupLayout bukuPanelLayout = new javax.swing.GroupLayout(bukuPanel);
        bukuPanel.setLayout(bukuPanelLayout);
        bukuPanelLayout.setHorizontalGroup(
            bukuPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(bukuPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(bukuPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(bukuPanelLayout.createSequentialGroup()
                        .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 538, Short.MAX_VALUE)
                        .addContainerGap())
                    .addGroup(bukuPanelLayout.createSequentialGroup()
                        .addComponent(jLabel18, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGap(297, 297, 297))
                    .addGroup(bukuPanelLayout.createSequentialGroup()
                        .addGroup(bukuPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel4)
                            .addGroup(bukuPanelLayout.createSequentialGroup()
                                .addGroup(bukuPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addGroup(bukuPanelLayout.createSequentialGroup()
                                        .addComponent(addBuku, javax.swing.GroupLayout.PREFERRED_SIZE, 76, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(editBuku, javax.swing.GroupLayout.PREFERRED_SIZE, 69, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(simpanBuku, javax.swing.GroupLayout.PREFERRED_SIZE, 77, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addComponent(cariTextBuku))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(bukuPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(hapusBuku, javax.swing.GroupLayout.PREFERRED_SIZE, 71, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(cariBuku, javax.swing.GroupLayout.PREFERRED_SIZE, 71, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(bukuPanelLayout.createSequentialGroup()
                                .addGroup(bukuPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel15)
                                    .addGroup(bukuPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                        .addComponent(jLabel17, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(jLabel16, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                                .addGap(18, 18, 18)
                                .addGroup(bukuPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(pengarangBuku, javax.swing.GroupLayout.DEFAULT_SIZE, 244, Short.MAX_VALUE)
                                    .addComponent(judulBuku)
                                    .addComponent(kodeBuku, javax.swing.GroupLayout.PREFERRED_SIZE, 145, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(penerbitBuku))))
                        .addGap(0, 0, Short.MAX_VALUE))))
        );
        bukuPanelLayout.setVerticalGroup(
            bukuPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(bukuPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel4)
                .addGap(18, 18, 18)
                .addGroup(bukuPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel15)
                    .addComponent(kodeBuku, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(bukuPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(judulBuku, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel18, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(5, 5, 5)
                .addGroup(bukuPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel16)
                    .addComponent(pengarangBuku, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(bukuPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel17)
                    .addComponent(penerbitBuku, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(31, 31, 31)
                .addGroup(bukuPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(addBuku)
                    .addComponent(editBuku)
                    .addComponent(simpanBuku)
                    .addComponent(hapusBuku))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 24, Short.MAX_VALUE)
                .addGroup(bukuPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cariTextBuku, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cariBuku))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 265, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        mainPanel.add(bukuPanel, "card3");

        pinjamPanel.setBackground(new java.awt.Color(204, 204, 0));

        jLabel5.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(255, 255, 255));
        jLabel5.setText("PEMINJAMAN");

        jLabel19.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel19.setForeground(new java.awt.Color(255, 255, 255));
        jLabel19.setText("Kode Buku");

        jLabel20.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel20.setForeground(new java.awt.Color(255, 255, 255));
        jLabel20.setText("NIM");

        jLabel21.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel21.setForeground(new java.awt.Color(255, 255, 255));
        jLabel21.setText("Judul Buku");

        jLabel22.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel22.setForeground(new java.awt.Color(255, 255, 255));
        jLabel22.setText("Tanggal Pinjam");

        addBukupinjam.setFont(new java.awt.Font("Times New Roman", 0, 12)); // NOI18N
        addBukupinjam.setText("Lihat");
        addBukupinjam.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addBukupinjamActionPerformed(evt);
            }
        });

        editBukupinjam.setFont(new java.awt.Font("Times New Roman", 0, 12)); // NOI18N
        editBukupinjam.setText("Edit");

        simpanBukupinjam.setFont(new java.awt.Font("Times New Roman", 0, 12)); // NOI18N
        simpanBukupinjam.setText("Simpan");

        hapusBukupinjam.setFont(new java.awt.Font("Times New Roman", 0, 12)); // NOI18N
        hapusBukupinjam.setText("Hapus");

        cariBukupinjam.setFont(new java.awt.Font("Times New Roman", 0, 12)); // NOI18N
        cariBukupinjam.setText("Cari");

        tabelPeminjam.setFont(new java.awt.Font("Times New Roman", 0, 12)); // NOI18N
        tabelPeminjam.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null}
            },
            new String [] {
                "Kode Buku", "Judul Buku", "NIM", "Tanggal Pinjam", "Tanggal Kembali"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }
        });
        jScrollPane4.setViewportView(tabelPeminjam);

        tanggalKembali.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        tanggalKembali.setForeground(new java.awt.Color(255, 255, 255));
        tanggalKembali.setText("Tanggal Kembali");

        javax.swing.GroupLayout pinjamPanelLayout = new javax.swing.GroupLayout(pinjamPanel);
        pinjamPanel.setLayout(pinjamPanelLayout);
        pinjamPanelLayout.setHorizontalGroup(
            pinjamPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pinjamPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pinjamPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pinjamPanelLayout.createSequentialGroup()
                        .addComponent(jLabel21, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGap(297, 297, 297))
                    .addGroup(pinjamPanelLayout.createSequentialGroup()
                        .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                        .addContainerGap())
                    .addGroup(pinjamPanelLayout.createSequentialGroup()
                        .addGroup(pinjamPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel5)
                            .addGroup(pinjamPanelLayout.createSequentialGroup()
                                .addGroup(pinjamPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addGroup(pinjamPanelLayout.createSequentialGroup()
                                        .addComponent(addBukupinjam, javax.swing.GroupLayout.PREFERRED_SIZE, 76, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(editBukupinjam, javax.swing.GroupLayout.PREFERRED_SIZE, 69, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(simpanBukupinjam, javax.swing.GroupLayout.PREFERRED_SIZE, 77, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addComponent(cariTextBukupinjam, javax.swing.GroupLayout.PREFERRED_SIZE, 242, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(pinjamPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(hapusBukupinjam, javax.swing.GroupLayout.PREFERRED_SIZE, 71, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(cariBukupinjam, javax.swing.GroupLayout.PREFERRED_SIZE, 71, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(pinjamPanelLayout.createSequentialGroup()
                                .addGroup(pinjamPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel19)
                                    .addGroup(pinjamPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                        .addComponent(jLabel22, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(jLabel20, javax.swing.GroupLayout.Alignment.LEADING)))
                                .addGap(18, 18, 18)
                                .addGroup(pinjamPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(penerbitBuku2, javax.swing.GroupLayout.PREFERRED_SIZE, 244, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGroup(pinjamPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                        .addComponent(NIMpeminjam)
                                        .addComponent(judulBukupinjam)
                                        .addComponent(kodeBukupinjam, javax.swing.GroupLayout.PREFERRED_SIZE, 145, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(tanggalPinjam, javax.swing.GroupLayout.PREFERRED_SIZE, 244, javax.swing.GroupLayout.PREFERRED_SIZE))))
                            .addComponent(tanggalKembali))
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
        );
        pinjamPanelLayout.setVerticalGroup(
            pinjamPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pinjamPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel5)
                .addGap(18, 18, 18)
                .addGroup(pinjamPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel19)
                    .addComponent(kodeBukupinjam, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pinjamPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(judulBukupinjam, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel21, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(5, 5, 5)
                .addGroup(pinjamPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel20)
                    .addComponent(NIMpeminjam, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pinjamPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel22)
                    .addComponent(tanggalPinjam, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pinjamPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(tanggalKembali)
                    .addComponent(penerbitBuku2, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(pinjamPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(addBukupinjam)
                    .addComponent(editBukupinjam)
                    .addComponent(simpanBukupinjam)
                    .addComponent(hapusBukupinjam))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(pinjamPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cariTextBukupinjam, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cariBukupinjam))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane4, javax.swing.GroupLayout.DEFAULT_SIZE, 259, Short.MAX_VALUE)
                .addContainerGap())
        );

        mainPanel.add(pinjamPanel, "card4");

        kembaliPanel.setBackground(new java.awt.Color(102, 102, 0));
        kembaliPanel.setForeground(new java.awt.Color(255, 255, 255));

        jLabel6.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(255, 255, 255));
        jLabel6.setText("PENGEMBALIAN");

        jLabel23.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel23.setForeground(new java.awt.Color(255, 255, 255));
        jLabel23.setText("Kode Buku");

        jLabel24.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel24.setForeground(new java.awt.Color(255, 255, 255));
        jLabel24.setText("Judul Buku");

        jLabel25.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel25.setForeground(new java.awt.Color(255, 255, 255));
        jLabel25.setText("NIM");

        jLabel26.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel26.setForeground(new java.awt.Color(255, 255, 255));
        jLabel26.setText("Tanggal Pinjam");

        tanggalKembali1.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        tanggalKembali1.setForeground(new java.awt.Color(255, 255, 255));
        tanggalKembali1.setText("Tanggal Kembali");

        addBukukembali.setFont(new java.awt.Font("Times New Roman", 0, 12)); // NOI18N
        addBukukembali.setText("Lihat");
        addBukukembali.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addBukukembaliActionPerformed(evt);
            }
        });

        editBukukembali.setFont(new java.awt.Font("Times New Roman", 0, 12)); // NOI18N
        editBukukembali.setText("Edit");

        simpanBukukembali.setFont(new java.awt.Font("Times New Roman", 0, 12)); // NOI18N
        simpanBukukembali.setText("Simpan");
        simpanBukukembali.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                simpanBukukembaliActionPerformed(evt);
            }
        });

        hapusBukukembali.setFont(new java.awt.Font("Times New Roman", 0, 12)); // NOI18N
        hapusBukukembali.setText("Hapus");

        cariBukukembali.setFont(new java.awt.Font("Times New Roman", 0, 12)); // NOI18N
        cariBukukembali.setText("Cari");

        tabelKembali.setFont(new java.awt.Font("Times New Roman", 0, 12)); // NOI18N
        tabelKembali.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null}
            },
            new String [] {
                "Kode Buku", "Judul Buku", "NIM", "Tanggal Pinjam", "Tanggal Kembali", "Status"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.Integer.class
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }
        });
        jScrollPane5.setViewportView(tabelKembali);

        tanggalKembali2.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        tanggalKembali2.setForeground(new java.awt.Color(255, 255, 255));
        tanggalKembali2.setText("Status");

        javax.swing.GroupLayout kembaliPanelLayout = new javax.swing.GroupLayout(kembaliPanel);
        kembaliPanel.setLayout(kembaliPanelLayout);
        kembaliPanelLayout.setHorizontalGroup(
            kembaliPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(kembaliPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(kembaliPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(kembaliPanelLayout.createSequentialGroup()
                        .addComponent(jLabel24, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGap(297, 297, 297))
                    .addGroup(kembaliPanelLayout.createSequentialGroup()
                        .addGroup(kembaliPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel6)
                            .addGroup(kembaliPanelLayout.createSequentialGroup()
                                .addGroup(kembaliPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addGroup(kembaliPanelLayout.createSequentialGroup()
                                        .addComponent(addBukukembali, javax.swing.GroupLayout.PREFERRED_SIZE, 76, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(editBukukembali, javax.swing.GroupLayout.PREFERRED_SIZE, 69, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(simpanBukukembali, javax.swing.GroupLayout.PREFERRED_SIZE, 77, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(4, 4, 4))
                                    .addComponent(cariTextBukukembali, javax.swing.GroupLayout.PREFERRED_SIZE, 242, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(kembaliPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(cariBukukembali, javax.swing.GroupLayout.PREFERRED_SIZE, 71, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(hapusBukukembali, javax.swing.GroupLayout.PREFERRED_SIZE, 71, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(kembaliPanelLayout.createSequentialGroup()
                                .addGroup(kembaliPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel23)
                                    .addGroup(kembaliPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                        .addComponent(jLabel26, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(jLabel25, javax.swing.GroupLayout.Alignment.LEADING)))
                                .addGap(18, 18, 18)
                                .addGroup(kembaliPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(tanggalkembaliin, javax.swing.GroupLayout.PREFERRED_SIZE, 244, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGroup(kembaliPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                        .addComponent(NIMkembali)
                                        .addComponent(judulBukukembali)
                                        .addComponent(kodeBukukembali, javax.swing.GroupLayout.PREFERRED_SIZE, 145, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(tanggalkembali, javax.swing.GroupLayout.PREFERRED_SIZE, 244, javax.swing.GroupLayout.PREFERRED_SIZE))))
                            .addComponent(tanggalKembali1))
                        .addGap(128, 199, Short.MAX_VALUE))
                    .addGroup(kembaliPanelLayout.createSequentialGroup()
                        .addComponent(tanggalKembali2, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(Denda, javax.swing.GroupLayout.PREFERRED_SIZE, 118, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, kembaliPanelLayout.createSequentialGroup()
                        .addComponent(jScrollPane5)
                        .addContainerGap())))
        );
        kembaliPanelLayout.setVerticalGroup(
            kembaliPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(kembaliPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel6)
                .addGap(18, 18, 18)
                .addGroup(kembaliPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel23)
                    .addComponent(kodeBukukembali, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(kembaliPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel24, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(judulBukukembali, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(kembaliPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel25)
                    .addComponent(NIMkembali, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(kembaliPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel26)
                    .addComponent(tanggalkembali, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(kembaliPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(tanggalKembali1)
                    .addComponent(tanggalkembaliin, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(kembaliPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(tanggalKembali2)
                    .addComponent(Denda, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(kembaliPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(addBukukembali)
                    .addComponent(editBukukembali)
                    .addComponent(simpanBukukembali)
                    .addComponent(hapusBukukembali))
                .addGap(18, 18, 18)
                .addGroup(kembaliPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cariTextBukukembali, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cariBukukembali))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane5, javax.swing.GroupLayout.DEFAULT_SIZE, 226, Short.MAX_VALUE)
                .addContainerGap())
        );

        mainPanel.add(kembaliPanel, "card5");

        laporPanel.setBackground(new java.awt.Color(153, 0, 102));

        jLabel7.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        jLabel7.setForeground(new java.awt.Color(255, 255, 255));
        jLabel7.setText("LAPORAN ");

        tabelLaporan.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null}
            },
            new String [] {
                "Kode Buku", "Judul Buku", "NIM", "Tanggal Pinjam", "Tanggal Kembali", "Status"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                true, true, true, true, false, true
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane6.setViewportView(tabelLaporan);

        javax.swing.GroupLayout laporPanelLayout = new javax.swing.GroupLayout(laporPanel);
        laporPanel.setLayout(laporPanelLayout);
        laporPanelLayout.setHorizontalGroup(
            laporPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(laporPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(laporPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(laporPanelLayout.createSequentialGroup()
                        .addComponent(jLabel7)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(jScrollPane6, javax.swing.GroupLayout.DEFAULT_SIZE, 538, Short.MAX_VALUE))
                .addContainerGap())
        );
        laporPanelLayout.setVerticalGroup(
            laporPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(laporPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel7)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(99, Short.MAX_VALUE))
        );

        mainPanel.add(laporPanel, "card6");

        anggotaPanel.setBackground(new java.awt.Color(0, 51, 102));

        jLabel8.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        jLabel8.setForeground(new java.awt.Color(255, 255, 255));
        jLabel8.setText("ANGGOTA");

        jLabel11.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel11.setForeground(new java.awt.Color(255, 255, 255));
        jLabel11.setText("Nama ");

        jLabel12.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel12.setForeground(new java.awt.Color(255, 255, 255));
        jLabel12.setText("NIM");

        nimAnggota.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                nimAnggotaActionPerformed(evt);
            }
        });

        jLabel13.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel13.setForeground(new java.awt.Color(255, 255, 255));
        jLabel13.setText("Username");

        jLabel14.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel14.setForeground(new java.awt.Color(255, 255, 255));
        jLabel14.setText("Password");

        passAnggota.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                passAnggotaActionPerformed(evt);
            }
        });

        addAnggota.setText("Lihat");
        addAnggota.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addAnggotaActionPerformed(evt);
            }
        });

        editAnggota.setText("Edit");
        editAnggota.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                editAnggotaActionPerformed(evt);
            }
        });

        simpanAnggota.setText("Simpan");
        simpanAnggota.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                simpanAnggotaActionPerformed(evt);
            }
        });

        hapusAnggota.setText("Hapus");
        hapusAnggota.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                hapusAnggotaActionPerformed(evt);
            }
        });

        cariAnggota.setText("Cari");
        cariAnggota.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cariAnggotaActionPerformed(evt);
            }
        });

        tabelAnggota.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Nama", "NIM", "Username", "Password"
            }
        ));
        jScrollPane2.setViewportView(tabelAnggota);

        javax.swing.GroupLayout anggotaPanelLayout = new javax.swing.GroupLayout(anggotaPanel);
        anggotaPanel.setLayout(anggotaPanelLayout);
        anggotaPanelLayout.setHorizontalGroup(
            anggotaPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(anggotaPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(anggotaPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(anggotaPanelLayout.createSequentialGroup()
                        .addGroup(anggotaPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(anggotaPanelLayout.createSequentialGroup()
                                .addComponent(jLabel8)
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addGroup(anggotaPanelLayout.createSequentialGroup()
                                .addGroup(anggotaPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(anggotaPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jLabel14, javax.swing.GroupLayout.PREFERRED_SIZE, 66, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jLabel13, javax.swing.GroupLayout.PREFERRED_SIZE, 66, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jLabel12, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 66, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addComponent(jLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, 66, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(anggotaPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(usernameAnggota)
                                    .addComponent(passAnggota)
                                    .addComponent(nimAnggota)
                                    .addComponent(namaAnggota))))
                        .addGap(134, 134, 134))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, anggotaPanelLayout.createSequentialGroup()
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 538, Short.MAX_VALUE)
                        .addContainerGap())
                    .addGroup(anggotaPanelLayout.createSequentialGroup()
                        .addGroup(anggotaPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(anggotaPanelLayout.createSequentialGroup()
                                .addComponent(addAnggota, javax.swing.GroupLayout.PREFERRED_SIZE, 74, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(editAnggota, javax.swing.GroupLayout.PREFERRED_SIZE, 72, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(simpanAnggota, javax.swing.GroupLayout.PREFERRED_SIZE, 77, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(hapusAnggota, javax.swing.GroupLayout.PREFERRED_SIZE, 76, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(anggotaPanelLayout.createSequentialGroup()
                                .addComponent(cariTextAnggota, javax.swing.GroupLayout.PREFERRED_SIZE, 243, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(cariAnggota, javax.swing.GroupLayout.PREFERRED_SIZE, 76, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(0, 0, Short.MAX_VALUE))))
        );
        anggotaPanelLayout.setVerticalGroup(
            anggotaPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(anggotaPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel8)
                .addGap(18, 18, 18)
                .addGroup(anggotaPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(namaAnggota, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel11))
                .addGap(8, 8, 8)
                .addGroup(anggotaPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel12)
                    .addComponent(nimAnggota, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(6, 6, 6)
                .addGroup(anggotaPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(usernameAnggota, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel13))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(anggotaPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(passAnggota, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel14))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 34, Short.MAX_VALUE)
                .addGroup(anggotaPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(addAnggota)
                    .addComponent(editAnggota)
                    .addComponent(simpanAnggota)
                    .addComponent(hapusAnggota))
                .addGap(18, 18, 18)
                .addGroup(anggotaPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cariTextAnggota, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cariAnggota))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 265, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        mainPanel.add(anggotaPanel, "card7");

        javax.swing.GroupLayout bodyPanelLayout = new javax.swing.GroupLayout(bodyPanel);
        bodyPanel.setLayout(bodyPanelLayout);
        bodyPanelLayout.setHorizontalGroup(
            bodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(bodyPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(menuPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(mainPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        bodyPanelLayout.setVerticalGroup(
            bodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, bodyPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(bodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(mainPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(menuPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(bodyPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(bodyPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void btnPinjamActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPinjamActionPerformed
        // TODO add your handling code here:
        getTablePeminjam();
         //remove panel
        mainPanel.removeAll();
        mainPanel.repaint();
        mainPanel.revalidate();
        //add panel
        mainPanel.add(pinjamPanel);
        mainPanel.repaint();
        mainPanel.revalidate();
    }//GEN-LAST:event_btnPinjamActionPerformed

    private void btnKembaliActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnKembaliActionPerformed
        // TODO add your handling code here:
        getTableKembali();
         //remove panel
        mainPanel.removeAll();
        mainPanel.repaint();
        mainPanel.revalidate();
        //add panel
        mainPanel.add(kembaliPanel);
        mainPanel.repaint();
        mainPanel.revalidate();
    }//GEN-LAST:event_btnKembaliActionPerformed

    private void btnLaporActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLaporActionPerformed
        // TODO add your handling code here:
        getTableLaporan();
         //remove panel
        mainPanel.removeAll();
        mainPanel.repaint();
        mainPanel.revalidate();
        //add panel
        mainPanel.add(laporPanel);
        mainPanel.repaint();
        mainPanel.revalidate();
    }//GEN-LAST:event_btnLaporActionPerformed

    private void btnLogoutActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLogoutActionPerformed
        // TODO add your handling code here:
        
        int dialogBtn = JOptionPane.YES_NO_OPTION;
        int dialogResult = JOptionPane.showConfirmDialog(this,"Anda yakin akan keluar?","PERINGATAN", dialogBtn);
        
        if(dialogResult==0){
            //true condition
            System.exit(0);
        }else{
            //false condition
        }
    }//GEN-LAST:event_btnLogoutActionPerformed

    private void btnBukuActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBukuActionPerformed
        getTableBuku();
        // TODO add your handling code here:
         //remove panel
        mainPanel.removeAll();
        mainPanel.repaint();
        mainPanel.revalidate();
        //add panel
        mainPanel.add(bukuPanel);
        mainPanel.repaint();
        mainPanel.revalidate();
    }//GEN-LAST:event_btnBukuActionPerformed

    private void btnuserActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnuserActionPerformed
        // TODO add your handling code here:
        getTablePetugas();
        //remove panel
        mainPanel.removeAll();
        mainPanel.repaint();
        mainPanel.revalidate();
        //add panel
        mainPanel.add(userPanel);
        mainPanel.repaint();
        mainPanel.revalidate();
    }//GEN-LAST:event_btnuserActionPerformed

    private void btnAnggotaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAnggotaActionPerformed
        getTableAnggota();
        // TODO add your handling code here:
        //remove panel
        mainPanel.removeAll();
        mainPanel.repaint();
        mainPanel.revalidate();
        //add panel
        mainPanel.add(anggotaPanel);
        mainPanel.repaint();
        mainPanel.revalidate();
    }//GEN-LAST:event_btnAnggotaActionPerformed

    private void passUserActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_passUserActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_passUserActionPerformed

    private void nipUserActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_nipUserActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_nipUserActionPerformed

    private void addUserActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addUserActionPerformed
      
    }//GEN-LAST:event_addUserActionPerformed

    private void nimAnggotaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_nimAnggotaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_nimAnggotaActionPerformed

    private void passAnggotaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_passAnggotaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_passAnggotaActionPerformed

    private void addAnggotaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addAnggotaActionPerformed
   
    }//GEN-LAST:event_addAnggotaActionPerformed

    private void namaUserActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_namaUserActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_namaUserActionPerformed

    private void addBukuActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addBukuActionPerformed
   
    }//GEN-LAST:event_addBukuActionPerformed

    private void addBukupinjamActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addBukupinjamActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_addBukupinjamActionPerformed

    private void addBukukembaliActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addBukukembaliActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_addBukukembaliActionPerformed

    private void simpanBukukembaliActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_simpanBukukembaliActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_simpanBukukembaliActionPerformed

    private void simpanUserActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_simpanUserActionPerformed
          
    }//GEN-LAST:event_simpanUserActionPerformed

    private void hapusUserActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_hapusUserActionPerformed
 
    }//GEN-LAST:event_hapusUserActionPerformed

    private void editUserActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_editUserActionPerformed
    
    }//GEN-LAST:event_editUserActionPerformed

    private void cariUserActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cariUserActionPerformed
    
    }//GEN-LAST:event_cariUserActionPerformed

    private void simpanAnggotaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_simpanAnggotaActionPerformed
      
    }//GEN-LAST:event_simpanAnggotaActionPerformed

    private void hapusAnggotaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_hapusAnggotaActionPerformed
       
    }//GEN-LAST:event_hapusAnggotaActionPerformed

    private void editAnggotaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_editAnggotaActionPerformed
        
    }//GEN-LAST:event_editAnggotaActionPerformed

    private void cariAnggotaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cariAnggotaActionPerformed
    
    }//GEN-LAST:event_cariAnggotaActionPerformed

    private void simpanBukuActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_simpanBukuActionPerformed
       
    }//GEN-LAST:event_simpanBukuActionPerformed

    private void hapusBukuActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_hapusBukuActionPerformed
  
    }//GEN-LAST:event_hapusBukuActionPerformed

    private void cariBukuActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cariBukuActionPerformed
       
    }//GEN-LAST:event_cariBukuActionPerformed

    private void editBukuActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_editBukuActionPerformed
      
    }//GEN-LAST:event_editBukuActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(MainView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(MainView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(MainView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MainView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new MainView().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField Denda;
    private javax.swing.JTextField NIMkembali;
    private javax.swing.JTextField NIMpeminjam;
    private javax.swing.JButton addAnggota;
    private javax.swing.JButton addBuku;
    private javax.swing.JButton addBukukembali;
    private javax.swing.JButton addBukupinjam;
    private javax.swing.JButton addUser;
    private javax.swing.JPanel anggotaPanel;
    private javax.swing.JPanel bodyPanel;
    private javax.swing.JButton btnAnggota;
    private javax.swing.JButton btnBuku;
    private javax.swing.JButton btnKembali;
    private javax.swing.JButton btnLapor;
    private javax.swing.JButton btnLogout;
    private javax.swing.JButton btnPinjam;
    private javax.swing.JButton btnuser;
    private javax.swing.JPanel bukuPanel;
    private javax.swing.JButton cariAnggota;
    private javax.swing.JButton cariBuku;
    private javax.swing.JButton cariBukukembali;
    private javax.swing.JButton cariBukupinjam;
    private javax.swing.JTextField cariTextAnggota;
    private javax.swing.JTextField cariTextBuku;
    private javax.swing.JTextField cariTextBukukembali;
    private javax.swing.JTextField cariTextBukupinjam;
    private javax.swing.JTextField cariTextUser;
    private javax.swing.JButton cariUser;
    private javax.swing.JButton editAnggota;
    private javax.swing.JButton editBuku;
    private javax.swing.JButton editBukukembali;
    private javax.swing.JButton editBukupinjam;
    private javax.swing.JButton editUser;
    private javax.swing.JButton hapusAnggota;
    private javax.swing.JButton hapusBuku;
    private javax.swing.JButton hapusBukukembali;
    private javax.swing.JButton hapusBukupinjam;
    private javax.swing.JButton hapusUser;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel27;
    private javax.swing.JLabel jLabel28;
    private javax.swing.JLabel jLabel29;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JScrollPane jScrollPane6;
    private javax.swing.JTextField judulBuku;
    private javax.swing.JTextField judulBukukembali;
    private javax.swing.JTextField judulBukupinjam;
    private javax.swing.JPanel kembaliPanel;
    private javax.swing.JTextField kodeBuku;
    private javax.swing.JTextField kodeBukukembali;
    private javax.swing.JTextField kodeBukupinjam;
    private javax.swing.JPanel laporPanel;
    private javax.swing.JPanel mainPanel;
    private javax.swing.JPanel menuPanel;
    private javax.swing.JTextField namaAnggota;
    private javax.swing.JTextField namaUser;
    private javax.swing.JTextField nimAnggota;
    private javax.swing.JTextField nipUser;
    private javax.swing.JPasswordField passAnggota;
    private javax.swing.JPasswordField passUser;
    private javax.swing.JTextField penerbitBuku;
    private javax.swing.JTextField penerbitBuku2;
    private javax.swing.JTextField pengarangBuku;
    private javax.swing.JPanel pinjamPanel;
    private javax.swing.JButton simpanAnggota;
    private javax.swing.JButton simpanBuku;
    private javax.swing.JButton simpanBukukembali;
    private javax.swing.JButton simpanBukupinjam;
    private javax.swing.JButton simpanUser;
    private javax.swing.JTable tabelAnggota;
    private javax.swing.JTable tabelBuku;
    private javax.swing.JTable tabelKembali;
    private javax.swing.JTable tabelLaporan;
    private javax.swing.JTable tabelPeminjam;
    private javax.swing.JTable tabelUser;
    private javax.swing.JLabel tanggalKembali;
    private javax.swing.JLabel tanggalKembali1;
    private javax.swing.JLabel tanggalKembali2;
    private javax.swing.JTextField tanggalPinjam;
    private javax.swing.JTextField tanggalkembali;
    private javax.swing.JTextField tanggalkembaliin;
    private javax.swing.JPanel userPanel;
    private javax.swing.JTextField usernameAdmin;
    private javax.swing.JTextField usernameAnggota;
    // End of variables declaration//GEN-END:variables

   
}
